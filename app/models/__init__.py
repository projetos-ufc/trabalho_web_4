"""Modelos do Sistema."""
from flask_marshmallow import Marshmallow
from flask_marshmallow.fields import fields
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from passlib.hash import pbkdf2_sha256
db = SQLAlchemy()
ma = Marshmallow()


def config_db_ma(app):
    """Configuração das bibliotecas no app."""
    db.init_app(app)
    ma.init_app(app)
    app.db = db
    Migrate(app, app.db)


cursos_disciplinas = db.Table(
    'cursos_disciplinas',
    db.Column(
        'cursos_id',
        db.Integer,
        db.ForeignKey(
            'cursos.id',
            onupdate="CASCADE",
            ondelete="SET NULL"
        ),
        nullable=False
    ),
    db.Column(
        'disciplinas_id',
        db.Integer,
        db.ForeignKey(
            'disciplinas.id',
            onupdate="CASCADE",
            ondelete="SET NULL"
        ),
        nullable=False
    )
)


class Cursos(db.Model):
    """Model das Disciplinas."""

    __tablename__ = "cursos"

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(100), nullable=False)
    descricao = db.Column(db.Text, nullable=False)
    disciplinas = db.relationship(
        'Disciplinas',
        secondary=cursos_disciplinas,
        lazy=True,
        backref=db.backref('cursos', lazy=True)
    )

    def __init__(self, nome=None, descricao=None, *args, **kwargs):
        """Inicializador do Model."""
        super().__init__(*args, **kwargs)
        self.nome = nome
        self.descricao = descricao

    def __repr__(self):
        """Representação do Model."""
        return '<Curso %r>' % self.nome


class Disciplinas(db.Model):
    """Model das Disciplinas."""

    __tablename__ = "disciplinas"

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(100), nullable=False)
    descricao = db.Column(db.Text, nullable=False)
    topicos = db.relationship('Topicos', backref='disciplina', lazy=True)

    def __init__(self, nome=None, descricao=None, *args, **kwargs):
        """Inicializador do Model."""
        super().__init__(*args, **kwargs)
        self.nome = nome
        self.descricao = descricao

    def __repr__(self):
        """Representação do Model."""
        return '<Disciplina %r>' % self.nome


class Topicos(db.Model):
    """Model dos Topicos."""

    __tablename__ = "topicos"

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(100), nullable=False)
    disciplinas_id = db.Column(
        db.Integer,
        db.ForeignKey(
            'disciplinas.id',
            onupdate="CASCADE",
            ondelete="SET NULL"
        ),
        nullable=False
    )

    def __init__(self, nome=None, *args, **kwargs):
        """Inicializador do Model."""
        super().__init__(*args, **kwargs)
        self.nome = nome

    def __repr__(self):
        """Representação do Model."""
        return '<Topico %r>' % self.nome


class Relacionamentos(db.Model):
    """Model dos Relacionamentos."""

    __tablename__ = 'relacionamentos'
    id = db.Column(db.Integer, primary_key=True)
    descricao = db.Column(db.Text, nullable=False)
    topicos1_id = db.Column(
        db.Integer,
        db.ForeignKey(
            'topicos.id',
            onupdate="CASCADE",
            ondelete="SET NULL"
        )
    )
    topicos2_id = db.Column(
        db.Integer,
        db.ForeignKey(
            'topicos.id',
            onupdate="CASCADE",
            ondelete="SET NULL"
        )
    )
    topicos1 = db.relationship(
        "Topicos",
        primaryjoin=Topicos.id == topicos1_id,
    )
    topicos2 = db.relationship(
        "Topicos",
        primaryjoin=Topicos.id == topicos2_id,
    )
    status = db.Column(db.Integer, nullable=False, default=0)

    def __init__(self, descricao=None, status='0', *args, **kwargs):
        """Inicializador do Model."""
        super().__init__(*args, **kwargs)
        self.descricao = descricao
        self.status = status

    def __repr__(self):
        """Representação do Model."""
        return '<Relacionamento %r>' % self.id


class Usuarios(db.Model):
    """Model dos Usuarios."""

    __tablename__ = "usuarios"

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(200), nullable=False)
    email = db.Column(db.String(200), unique=True, nullable=False)
    senha = db.Column(db.String(200), nullable=False)
    level = db.Column(db.Integer, nullable=False, default="1")

    def __init__(self, nome=None, email=None, senha=None, level=1, *args, **kwargs):
        """Inicializador do Model."""
        super().__init__(*args, **kwargs)
        self.nome = nome
        self.email = email
        self.senha = senha
        self.level = level

    def hash_senha(self):
        """Criptografa Senha."""
        self.senha = pbkdf2_sha256.hash(self.senha)

    def verificar_senha(self, senha):
        """Verifica se a Senha está Correta."""
        return pbkdf2_sha256.verify(senha, self.senha)

    def __repr__(self):
        """Representação do Model."""
        return '<User %r>' % self.email


class CursosIDSchema(ma.Schema):
    """Schema para serialização dos cursos simplificado."""

    id = fields.Integer(dump_only=True)
    nome = fields.Str(dump_only=True)


class TopicosIDSchema(ma.Schema):
    """Schema para serialização dos topicos simplificado."""

    id = fields.Integer(dump_only=True)
    nome = fields.Str(dump_only=True)


class DisciplinasIDSchema(ma.Schema):
    """Schema para serialização das disciplinas simplificada."""

    id = fields.Integer(dump_only=True)
    nome = fields.Str(dump_only=True)


class UsuariosSchema(ma.Schema):
    """Schema para serialização do Usuarios."""

    class Meta:
        """Metadados do Schema."""

        model = Usuarios

    id = fields.Integer(dump_only=True)
    nome = fields.Str()
    email = fields.Str(required=True)
    senha = fields.Str(required=True, load_only=True)
    level = fields.Integer()


class CursosSchema(ma.Schema):
    """Schema para serialização dos cursos."""

    class Meta:
        """Metadados do Schema."""

        model = Cursos

    id = fields.Integer(dump_only=True)
    nome = fields.Str(required=True)
    descricao = fields.Str(required=True)
    disciplinas = fields.List(fields.Nested(DisciplinasIDSchema), dump_only=True)


class DisciplinasSchema(ma.Schema):
    """Schema para serialização das disciplinas."""

    class Meta:
        """Metadados do Schema."""

        model = Disciplinas

    id = fields.Integer(dump_only=True)
    nome = fields.Str(required=True)
    descricao = fields.Str(required=True)
    cursos_id = fields.List(fields.Integer(), required=True, load_only=True)
    cursos = fields.List(fields.Nested(CursosIDSchema), dump_only=True)
    topicos = fields.List(fields.Nested(TopicosIDSchema), dump_only=True)


class TopicosSchema(ma.Schema):
    """Schema para serialização dos topicos."""

    class Meta:
        """Metadados do Schema."""

        model = Topicos

    id = fields.Integer(dump_only=True)
    nome = fields.Str(required=True)
    disciplinas_id = fields.Integer(required=True, load_only=True)
    disciplina = fields.Nested(DisciplinasIDSchema, allow_none=True)


class RelacionamentosSchema(ma.Schema):
    """Schema para serialização dos relacionamentos."""

    class Meta:
        """Metadados do Schema."""

        model = Relacionamentos

    id = fields.Integer(dump_only=True)
    descricao = fields.Str(required=True)
    topicos1_id = fields.Integer(required=True, load_only=True)
    topicos2_id = fields.Integer(required=True, load_only=True)
    topicos1 = fields.Nested(TopicosIDSchema, allow_none=True, dump_only=True)
    topicos2 = fields.Nested(TopicosIDSchema, allow_none=True, dump_only=True)
    status_relacionamento = fields.Integer(data_key='status', load_only=True)
    status = fields.Method("verificaStatus", dump_only=True)

    def verificaStatus(self, obj):
        """."""
        if obj.status == 1:
            return "Aceito"
        elif obj.status == 2:
            return "Rejeitado"
        else:
            return "Em espera"
