"""ReUsuarios dos Usuarios."""
from flask import current_app
from flask_restful import Resource
from flask_restful.reqparse import RequestParser
from flask_jwt_extended import jwt_required
from ..models import Usuarios, UsuariosSchema
from marshmallow.exceptions import ValidationError
from . import verificaNivel
from sqlalchemy.exc import IntegrityError


class UsuarioBase(Resource):
    """Classe Base das Disciplinas."""

    parser = RequestParser()
    parser.add_argument("nome")
    parser.add_argument("email")
    parser.add_argument("senha")
    parser.add_argument("level", default=1)


class UsuarioTodos(UsuarioBase):
    """Classe para Mostrar Todos os Usuarios."""

    @jwt_required
    @verificaNivel
    def get(self):
        """Retorna todas os Usuarios."""
        return {
            'usuarios': UsuariosSchema(
                many=True
            ).dump(Usuarios.query.all())}


class Usuario(UsuarioBase):
    """Classe Para Executar CRUD dos Usuarios."""

    @jwt_required
    @verificaNivel
    def get(self, id=None):
        """Read de um usuario."""
        if id == None:
            return {"status": "usuario invalido!"}, 400
        usuario = Usuarios.query.get(id)
        return ({'usuario': UsuariosSchema().dump(usuario)}, 200) if usuario else \
            ({"status": "usuario não encontrado!"}, 400)

    @jwt_required
    @verificaNivel
    def post(self, id=None):
        """Create de um usuario."""
        try:
            if id:
                return {"status": "erro não envie nenhum id!"}, 400
            dados = UsuariosSchema().load(self.parser.parse_args())
            usuario = Usuarios(**dados)
            usuario.hash_senha()
            try:
                current_app.db.session.add(usuario)
                current_app.db.session.commit()
                return {'status': 'Usuario Cadastrado com Sucesso!', 'id': usuario.id}, 201
            except IntegrityError:
                return {"status": "Email já Cadastrado!"}, 400
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400

    @jwt_required
    @verificaNivel
    def put(self, id=None):
        """Update de um usuario."""
        try:
            if id == None:
                return {"status": "usuario invalida!"}, 400
            dados = UsuariosSchema().load(self.parser.parse_args())
            usuario = Usuarios.query.get(id)
            if usuario:
                usuario.nome = dados['nome']
                usuario.email = dados['email']
                usuario.senha = dados['senha']
                usuario.level = dados['level']
                usuario.hash_senha()
                try:
                    current_app.db.session.add(usuario)
                    current_app.db.session.commit()
                    return {
                        "status": "atualizado!",
                        "usuario": UsuariosSchema().dump(usuario)
                    }
                except IntegrityError:
                    return {"status": "Email já está em uso!"}, 400
            return {"status": "usuario não encontrado!"}, 400
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400

    @jwt_required
    @verificaNivel
    def delete(self, id=None):
        """Delete de um usuario."""
        if id == None:
            return {"status": "usuario invalido!"}, 400
        usuario = Usuarios.query.get(id)
        if usuario:
            current_app.db.session.delete(usuario)
            current_app.db.session.commit()
            return {
                "status": "deletado!"
            }
        return {"status": "erro usuario não encontrado!"}
