"""Recursos das Diciplinas."""
from flask import current_app
from flask_restful import Resource
from flask_restful.reqparse import RequestParser
from flask_jwt_extended import jwt_required
from ..models import Disciplinas, DisciplinasSchema, Cursos
from marshmallow.exceptions import ValidationError
from . import verificaNivel


class DisciplinaBase(Resource):
    """Classe Base das Disciplinas."""

    parser = RequestParser()
    parser.add_argument("nome")
    parser.add_argument("descricao")
    parser.add_argument("cursos_id", type=list, location="json")


class DisciplinaTodas(DisciplinaBase):
    """Classe para Mostrar Todas Disciplinas."""

    @jwt_required
    def get(self):
        """Retorna todas as Disciplinas."""
        return {
            'disciplinas': DisciplinasSchema(
                only=('id', 'nome', 'cursos', 'descricao'),
                many=True
            ).dump(Disciplinas.query.all())
        }


class Disciplina(DisciplinaBase):
    """Classe Para Executar CRUD das Disciplinas."""

    @jwt_required
    def get(self, id=None):
        """Read de uma disciplina."""
        if id == None:
            return {"status": "disciplina invalida!"}, 400
        disciplina = Disciplinas.query.get(id)
        return ({'disciplina': DisciplinasSchema().dump(disciplina)}, 200) if disciplina else \
            ({"status": "disciplina não encontrada!"}, 400)

    @jwt_required
    @verificaNivel
    def post(self, id=None):
        """Create de uma disciplina."""
        try:
            if id:
                return {"status": "erro não envie o id de uma disciplina!"}, 400
            dados = DisciplinasSchema().load(self.parser.parse_args())
            c_ids = dados['cursos_id']
            del dados['cursos_id']
            cursos = list(
                filter(
                    lambda x: not (x is None),
                    map(
                        lambda x: Cursos.query.get(x),
                        c_ids
                    )
                )
            )
            if len(cursos) > 0:
                disciplina = Disciplinas(**dados)
                disciplina.cursos.extend(cursos)
                current_app.db.session.add(disciplina)
                current_app.db.session.commit()
                return {
                    "status": "cadastrado!",
                    "disciplina": DisciplinasSchema(
                        only=('id', 'nome', 'cursos', 'descricao'),
                    ).dump(disciplina)
                }, 201
            return {"status": "curso não encontrado!"}, 400
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400

    @jwt_required
    @verificaNivel
    def put(self, id=None):
        """Update de uma disciplina."""
        try:
            if id == None:
                return {"status": "disciplina invalida!"}, 400
            dados = DisciplinasSchema().load(self.parser.parse_args())
            disciplina = Disciplinas.query.get(id)
            if disciplina:
                c_ids = dados['cursos_id']
                cursos = list(
                    filter(
                        lambda x: not (x is None),
                        map(
                            lambda x: Cursos.query.get(x),
                            c_ids
                        )
                    )
                )
                if len(cursos) > 0:
                    disciplina.nome = dados['nome']
                    disciplina.descricao = dados['descricao']
                    disciplina.cursos.clear()
                    disciplina.cursos.extend(cursos)
                    current_app.db.session.add(disciplina)
                    current_app.db.session.commit()
                    return {
                        "status": "atualizado!",
                        "disciplina": DisciplinasSchema(
                            only=('id', 'nome', 'cursos', 'descricao'),
                        ).dump(disciplina)
                    }
                return {"status": "nenhum curso valido!"}, 400
            return {"status": "disciplina não encontrada!"}, 400
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400

    @jwt_required
    @verificaNivel
    def delete(self, id=None):
        """Delete de uma disciplina."""
        if id == None:
            return {"status": "disciplina invalida!"}, 400
        disciplina = Disciplinas.query.get(id)
        if disciplina:
            for i in disciplina.topicos:
                current_app.db.session.delete(i)
                current_app.db.session.commit()
            current_app.db.session.delete(disciplina)
            current_app.db.session.commit()
            return {
                "status": "deletado!"
            }
        return {"status": "erro disciplina não encontrada!"}
