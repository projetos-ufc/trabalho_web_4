"""."""
from flask import current_app
from flask_restful import Resource, abort
from flask_jwt_extended import create_access_token, get_jwt_identity
from flask_restful.reqparse import RequestParser
from marshmallow.exceptions import ValidationError
from ..models import UsuariosSchema, Usuarios
from sqlalchemy.exc import IntegrityError
import datetime


def verificaNivel(func):
    """Verificação do nivel de usuario."""
    def verifica(*args, **kwargs):
        user = Usuarios.query.get(get_jwt_identity())
        if user and user.level != 1:
            abort(403, status="Você não possui permissão para acessar!")
        return func(*args, **kwargs)
    return verifica


class Login(Resource):
    """."""

    parser = RequestParser()
    parser.add_argument("email")
    parser.add_argument("senha")

    def post(self):
        """."""
        try:
            dados = UsuariosSchema().load(self.parser.parse_args())
            usuario = Usuarios.query.filter_by(email=dados['email']).first()
            if usuario and usuario.verificar_senha(dados['senha']):
                time = datetime.timedelta(days=1)
                return {"JWT": create_access_token(usuario.id, expires_delta=time)}
            return {"status": 'Usuario não Existe ou Senha Incorreta!'}
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400
