"""Recursos dos Topicos."""
from flask import current_app
from flask_restful import Resource
from flask_restful.reqparse import RequestParser
from flask_jwt_extended import jwt_required
from ..models import Topicos, TopicosSchema, Disciplinas
from marshmallow.exceptions import ValidationError
from . import verificaNivel


class TopicoBase(Resource):
    """Classe Base dos Topicos."""

    parser = RequestParser()
    parser.add_argument("nome")
    parser.add_argument("disciplinas_id")


class TopicoTodos(TopicoBase):
    """Classe para Mostrar Todos Topicos."""

    @jwt_required
    def get(self):
        """Retorna todos os Topicos."""
        return {"topicos": TopicosSchema(many=True).dump(Topicos.query.all())}


class Topico(TopicoBase):
    """Classe Para Executar CRUD dos Topicos."""

    @jwt_required
    def get(self, id=None):
        """Read de um topico."""
        if id == None:
            return {"status": "topico invalido!"}, 400
        topico = Topicos.query.get(id)
        return ({"topico": TopicosSchema().dump(topico)}, 200) if topico else\
            ({"status": "topico não encontrado!"}, 400)

    @jwt_required
    @verificaNivel
    def post(self, id=None):
        """Create de um topico."""
        try:
            if id:
                return {"status": "erro não envie o id de um topico!"}, 400
            dados = TopicosSchema().load(self.parser.parse_args())
            disciplina = Disciplinas.query.get(dados['disciplinas_id'])
            if disciplina:
                topico = Topicos(**dados)
                topico.disciplina = disciplina
                current_app.db.session.add(topico)
                current_app.db.session.commit()
                return {
                    "status": "cadastrado!",
                    "topico": TopicosSchema().dump(topico)
                }, 201
            return {"status": "disciplina não encontrada!"}, 400
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400

    @jwt_required
    @verificaNivel
    def put(self, id=None):
        """Update de um topico."""
        try:
            if id == None:
                return {"status": "topico invalido!"}, 400
            dados = TopicosSchema().load(self.parser.parse_args())
            topico = Topicos.query.get(id)
            if topico:
                topico.nome = dados['nome']
                if topico.disciplinas_id != dados['disciplinas_id']:
                    disciplina = Disciplinas.query.get(dados['disciplinas_id'])
                    topico.disciplina = disciplina
                current_app.db.session.add(topico)
                current_app.db.session.commit()
                return {
                    "status": "Atualizado!",
                    "topico": TopicosSchema().dump(topico)
                }, 200
            return {"status": "Topico não encontrada!"}, 400
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400

    @jwt_required
    @verificaNivel
    def delete(self, id=None):
        """Delete de um topico."""
        if id == None:
            return {"status": "topico invalido!"}, 400
        topico = Topicos.query.get(id)
        if topico:
            current_app.db.session.delete(topico)
            current_app.db.session.commit()
            return {
                "status": "deletado!"
            }
        return {"status": "erro topico não encontrado!"}, 400
