"""Recursos dos Cursos."""
from flask import current_app
from flask_restful import Resource
from flask_restful.reqparse import RequestParser
from flask_jwt_extended import jwt_required
from ..models import Cursos, CursosSchema
from marshmallow.exceptions import ValidationError
from . import verificaNivel


class CursoBase(Resource):
    """Classe Base das Disciplinas."""

    parser = RequestParser()
    parser.add_argument("nome")
    parser.add_argument("descricao")


class CursoTodos(CursoBase):
    """Classe para Mostrar Todos os cursos."""

    @jwt_required
    def get(self):
        """Retorna todas os cursos."""
        return {
            'cursos': CursosSchema(
                only=("id", 'nome', 'descricao'),
                many=True
            ).dump(Cursos.query.all())}


class Curso(CursoBase):
    """Classe Para Executar CRUD dos cursos."""

    @jwt_required
    def get(self, id=None):
        """Read de um curso."""
        if id == None:
            return {"status": "curso invalido!"}, 400
        curso = Cursos.query.get(id)
        return ({'curso': CursosSchema().dump(curso)}, 200) if curso else \
            ({"status": "curso não encontrado!"}, 400)

    @jwt_required
    @verificaNivel
    def post(self, id=None):
        """Create de um curso."""
        try:
            if id:
                return {"status": "erro não envie nenhum id!"}, 400
            dados = CursosSchema().load(self.parser.parse_args())
            curso = Cursos(**dados)
            current_app.db.session.add(curso)
            current_app.db.session.commit()
            return {
                "status": "cadastrado!",
                "curso": CursosSchema(
                    only=("id", 'nome', 'descricao')
                ).dump(curso)
            }, 201
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400

    @jwt_required
    @verificaNivel
    def put(self, id=None):
        """Update de um curso."""
        try:
            if id == None:
                return {"status": "curso invalida!"}, 400
            dados = CursosSchema().load(self.parser.parse_args())
            curso = Cursos.query.get(id)
            if curso:
                curso.nome = dados['nome']
                curso.descricao = dados['descricao']
                current_app.db.session.add(curso)
                current_app.db.session.commit()
                return {
                    "status": "atualizado!",
                    "curso": CursosSchema(
                        only=("id", 'nome', 'descricao')
                    ).dump(curso)
                }
            return {"status": "curso não encontrado!"}, 400
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400

    @jwt_required
    @verificaNivel
    def delete(self, id=None):
        """Delete de um curso."""
        if id == None:
            return {"status": "curso invalido!"}, 400
        curso = Cursos.query.get(id)
        if curso:
            current_app.db.session.delete(curso)
            current_app.db.session.commit()
            return {
                "status": "deletado!"
            }
        return {"status": "erro curso não encontrado!"}
