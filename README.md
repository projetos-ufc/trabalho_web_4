# Trabalho de WEB 4
Trabalho da disciplina de web, api rest com flask.

## Bibliotecas Utilizadas
    Flask
    Flask Restful
    Flask SQLAlchemy
    Flask Migrate
    Flask JWT Extended
    Flask Marshmallow
    Decouple
    passlib
    dotenv
    Marshmallow SQLAlchemy