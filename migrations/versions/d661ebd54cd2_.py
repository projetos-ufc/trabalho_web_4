"""empty message

Revision ID: d661ebd54cd2
Revises: 
Create Date: 2019-12-01 07:33:31.369065

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd661ebd54cd2'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('cursos',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('nome', sa.String(length=100), nullable=False),
    sa.Column('descricao', sa.Text(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('disciplinas',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('nome', sa.String(length=100), nullable=False),
    sa.Column('descricao', sa.Text(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('usuarios',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('nome', sa.String(length=200), nullable=False),
    sa.Column('email', sa.String(length=200), nullable=False),
    sa.Column('senha', sa.String(length=200), nullable=False),
    sa.Column('level', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email')
    )
    op.create_table('cursos_disciplinas',
    sa.Column('cursos_id', sa.Integer(), nullable=False),
    sa.Column('disciplinas_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['cursos_id'], ['cursos.id'], onupdate='CASCADE', ondelete='SET NULL'),
    sa.ForeignKeyConstraint(['disciplinas_id'], ['disciplinas.id'], onupdate='CASCADE', ondelete='SET NULL')
    )
    op.create_table('topicos',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('nome', sa.String(length=100), nullable=False),
    sa.Column('disciplinas_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['disciplinas_id'], ['disciplinas.id'], onupdate='CASCADE', ondelete='SET NULL'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('relacionamentos',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('descricao', sa.Text(), nullable=False),
    sa.Column('topicos1_id', sa.Integer(), nullable=True),
    sa.Column('topicos2_id', sa.Integer(), nullable=True),
    sa.Column('status', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['topicos1_id'], ['topicos.id'], onupdate='CASCADE', ondelete='SET NULL'),
    sa.ForeignKeyConstraint(['topicos2_id'], ['topicos.id'], onupdate='CASCADE', ondelete='SET NULL'),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('relacionamentos')
    op.drop_table('topicos')
    op.drop_table('cursos_disciplinas')
    op.drop_table('usuarios')
    op.drop_table('disciplinas')
    op.drop_table('cursos')
    # ### end Alembic commands ###
